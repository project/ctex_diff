<?php

/**
 * @file
 * Page callbacks and general helper functions for CTools Export object Diff.
 */

/**
 * Provides an overview of CTools object types + exportable entity types.
 *
 * @return array
 *   Render array.
 */
function ctex_diff_overview() {
  $files = system_rebuild_module_data();
  $types = array();

  ctools_include('export');
  // List all exportables, not only 'bulk exportables', just to be able to see
  // what's going on in the system.
  $schemas = ctools_export_get_schemas();
  foreach ($schemas as $table => $schema) {
    $types[$table] = array(
      l($table, "admin/structure/ctex_diff/list/$table"),
      isset($files[$schema['module']]->info['name']) ? $files[$schema['module']]->info['name'] : '?',
      'CTools' . (empty($schema['export']['bulk export']) ? ' (' . t('not bulk exportable') . ')' : '')
    );
  }

  foreach (entity_get_info() as $entity_type => $entity_info) {
    if (!empty($entity_info['exportable'])) {
      if (isset($types[$entity_type])) {
        // Just give a warning; the code should probably be adjusted if this is
        // a real issue. All below code checks entities first, so keep those.
        drupal_set_message(t('Entity type @type also has a CTools exportable type with the same name; the latter will be invisible.'), 'warning');
      }
      $types[$entity_type] = array(
        l($entity_type, "admin/structure/ctex_diff/list/$entity_type"),
        isset($entity_info['plural label']) ? $entity_info['plural label'] : (isset($entity_info['label']) ? $entity_info['label'] : '?'),
        'Entity API',
      );
    }
  }

  ksort($types);
  return array(
    '#theme' => 'table',
    '#header' => array(t('Machine name'), t('Name / Module'), t('Type')),
    '#rows' => array_values($types),
  );
}

/**
 * Lists all 'direct CTools' objects of a certain type, with operations.
 *
 * @param string $type
 *   Type/table name.
 *
 * @return array
 *   Render array.
 */
function ctex_diff_list($type) {
  $header = array(t('Items'), t('Status'), t('Export type'), t('Operations'));
  $rows = array();

  $info = entity_get_info($type);
  if (empty($info['exportable'])) {
    // List CTools objects.
    ctools_include('export');
    $schema = ctools_export_get_schema($type);
    if (!$schema) {
      return array('#markup' => t('No CTools Export schema found for @type.', array('@type' => $type)));
    }

    $has_status = !empty($schema['export']['can disable']);
    if (!empty($schema['export']['list callback']) && function_exists($schema['export']['list callback'])) {
      $list = $schema['export']['list callback']();
    }
    else {
      $list = ctools_export_default_list($type, $schema);
    }

    $items = ctools_export_crud_load_all($type);
    uksort($items, 'strnatcasecmp');

    if (!$has_status) {
      unset($header[1]);
    }

    foreach ($items as $name => $item) {

      $links = array();
      $view_label = t('View');
      $delete_label = t('Delete');
      if ($item->export_type & EXPORT_IN_CODE) {
        if ($item->export_type & EXPORT_IN_DATABASE) {
          // Let's output Diff first, because that's what many people install
          // this module for.
          $links[] = l(t('Diff'), "admin/structure/ctex_diff/diff/$type/$name");
          // Set our label.
          $view_label = t('Default');
          $delete_label = t('Revert');
        }
        $links[] = l($view_label, "admin/structure/ctex_diff/code/$type/$name");
        // Set label for db.
        $view_label = t('Overridden');
      }
      if ($item->export_type & EXPORT_IN_DATABASE) {
        $links[] = l($view_label, "admin/structure/ctex_diff/current/$type/$name");
        // Don't allow messing with non bulk exportable types.
        if (!empty($schema['export']['bulk export'])) {
          $links[] = l($delete_label, "admin/structure/ctex_diff/delete/$type/$name", array('query' => array('destination' => $_GET['q'])));
        }
      }
      if ($has_status) {
        if (empty($item->disabled)) {
          $links[] = l(t('Disable'), "admin/structure/ctex_diff/disable/$type/$name", array('query' => array('destination' => $_GET['q'])));
        }
        else {
          $links[] = l(t('Enable'), "admin/structure/ctex_diff/enable/$type/$name", array('query' => array('destination' => $_GET['q'])));
        }
      }

      $row = array(
        isset($list[$name]) ? $list[$name] : $name,
        empty($item->disabled) ? t('enabled') : t('disabled'),
        $item->{$schema['export']['export type string']},
        implode(' | ', $links),
      );
      if (!$has_status) {
        unset($row[1]);
      }
      $rows[] = $row;
    }
  }
  else {
    // List entities.
    unset($header[1]);
    $statuskey = isset($info['entity keys']['status']) ? $info['entity keys']['status'] : 'status';
    // CTools exportables have a string inside the object itself; we need to
    // make a mapping.
    $statusmap = array(
      ENTITY_CUSTOM => t('Custom'),
      ENTITY_IN_CODE => t('Default'),
      ENTITY_OVERRIDDEN => t('Overridden'),
      ENTITY_FIXED => t('Fixed'),
    );

    // This is theoretically dangerous but we're just going to assume that there
    // are not a huge amount of these 'configuration entities'.
    $items = entity_load_multiple_by_name($type);
    uksort($items, 'strnatcasecmp');

    foreach ($items as $name => $item) {
      $links = array();
      $view_label = t('View');
      $delete_label = t('Delete');
      if ($item->$statuskey & ENTITY_IN_CODE) {
        if ($item->$statuskey & ENTITY_CUSTOM) {
          // Let's output Diff first, because that's what many people install
          // this module for.
          $links[] = l(t('Diff'), "admin/structure/ctex_diff/diff/$type/$name");
          // Set our label.
          $view_label = t('Default');
          $delete_label = t('Revert');
        }
        $links[] = l($view_label, "admin/structure/ctex_diff/code/$type/$name");
        // Set label for db.
        $view_label = t('Overridden');
      }
      if ($item->$statuskey & ENTITY_CUSTOM) {
        $links[] = l($view_label, "admin/structure/ctex_diff/current/$type/$name");
        $links[] = l($delete_label, "admin/structure/ctex_diff/delete/$type/$name", array('query' => array('destination' => $_GET['q'])));
      }

      $rows[] = array(
        $name,
        isset($statusmap[$item->$statuskey]) ? $statusmap[$item->$statuskey] : '?',
        implode(' | ', $links),
      );
    }
  }

  return array(
    array(
      '#markup' => t("Note that status 'Overridden' means objects are present in code as well as in the database. It does not mean they are necessarily different. If the 'Diff' links shows no difference, this means the values are the same and the object can be safely reverted."),
      '#prefix' => '<div class="help"><p>',
      '#suffix' => '</p></div>',
    ),
    array(
      '#theme' => 'table',
      '#header' => $header,
      '#rows' => $rows,
    ),
  );
}

/**
 * Outputs diff between default & current objects.
 *
 * @param string $type
 *   Entity type, or table name for 'non entity, direct CTools' objects.
 * @param object|string $object_or_name
 *   (Name of) the entity / CTools object.
 *
 * @return null|string
 *   HTML output containing diff.
 */
function ctex_diff_view_diff($type, $object_or_name) {

  $status = ctex_diff_get_status($type, $object_or_name);
  if (!isset($status)) {
    // trigger_error() was already called but we still need to output something.
    if (!is_object($object_or_name)) {
      // The object probably does not exist, which we'll want to output; we want
      // to make sure though. (The disadvantage is that trigger_error() will
      // likely be called twice but we'll live with that.)
      $object_or_name = ctex_diff_get_object($type, $object_or_name);
    }
    if (isset($object_or_name)) {
      return t('Status not set for object.');
    }
    else {
      return t('The object does not seem to exist.');
    }
  }
  if ($status == EXPORT_IN_CODE) { // == ENTITY_IN_CODE
    return t('Object exists only in code; cannot compare.');
  }
  if ($status == EXPORT_IN_DATABASE) { // == ENTITY_CUSTOM
    return t('Object exists only in database; cannot compare.');
  }
  if ($status != (EXPORT_IN_CODE | EXPORT_IN_DATABASE)) { // same as ENTITY_OVERRIDDEN
    return t('Strange export type/status @s!', array('@s' => $status));
  }

  // Largely copied from features_feature_diff():

  module_load_include('inc', 'diff', 'diff.engine');
  $formatter = new DrupalDiffFormatter();
  $diff = new Diff(
    explode("\n", ctex_diff_get_object($type, $object_or_name, TRUE, TRUE)),
    explode("\n", ctex_diff_get_object($type, $object_or_name, FALSE, TRUE))
  );
  $rows = $formatter->format($diff);
  $header = array(
    array('data' => t('Default'), 'colspan' => 2),
    array('data' => t('Overridden'), 'colspan' => 2),
  );

  drupal_add_css('table.diff td div {white-space: pre;}', array('type' => 'inline', 'preprocess' => FALSE));
  return theme('table', array('header' => $header, 'rows' => $rows, 'attributes' => array('class' => array('diff', 'features-diff'))));
}

/**
 * Outputs a serialized version of a default object (stored in code).
 *
 * This is a menu callback from our own 'code' menu path.
 *
 * @param string $type
 *   Entity type, or table name for 'non entity, direct CTools' objects.
 * @param object|string $object_or_name
 *   (Name of) the entity / CTools object.
 *
 * @return array
 *   Simple export form (which can serve as render array / page output).
 */
function ctex_diff_view_code($type, $object_or_name) {
  ctools_include('export');
  $serialized = ctex_diff_get_object($type, $object_or_name, TRUE, TRUE);
  return isset($serialized)
    ? drupal_get_form('ctools_export_form', $serialized, t('View default'))
    : t('The object does not seem to exist.');
}

/**
 * Output serialized version of object in its current form.
 *
 * 'Current form' means the object as passed in, or the version stored in the
 * database if $object_or_name is a name.
 *
 * This is a menu callback from our own menu path.
 *
 * @param string $type
 *   Table name for 'non entity, direct CTools' objects.
 * @param object|string $object_or_name
 *   (Name of) the entity / CTools object.
 *
 * @return string
 *   Simple export form (which can serve as render array / page output).
 */
function ctex_diff_view_current($type, $object_or_name) {
  // This is only referred to from the links in 'bulk export' pages (which
  // passes CTools object names); it automatically supports entities and objects
  // but that's not actually used.
  ctools_include('export');
  $serialized = ctex_diff_get_object($type, $object_or_name, FALSE, TRUE);
  return isset($serialized)
    ? drupal_get_form('ctools_export_form', $serialized, t('Database / Export'))
    : t('The object does not seem to exist.');
}

/**
 * Gets an object/entity.
 *
 * This helper function abstracts away some logic so we don't repeat modified
 * versions of the same code everywhere.
 *
 * @param string $type
 *   Entity type, or table name for 'non entity, direct CTools' objects.
 * @param object|string $object_or_name
 *   (Name of) the entity / CTools object.
 * @param bool $default_state
 *   (Optional) If true, returns object in its default state, even if it's
 *   overridden. If false (by default) returns the current object as passed in
 *   $object_or_name, or the object in 'overridden' state if $object_or_name is
 *   a string.
 * @param bool $serialized
 *   (Optional) If true, return serialized version of the object.
 *
 * @return object|string|null
 *   Object or serialized string on success, depending on $serialized. Null on
 *   failure, in which case trigger_error() will have been called.
 */
function ctex_diff_get_object($type, $object_or_name, $default_state = FALSE, $serialized = FALSE) {

  // Assume name by default; override later if object.
  $object_name = $object_or_name;

  $info = entity_get_info($type);

  if (empty($info['exportable'])) {
    // Handle CTools object.
    ctools_include('export');

    if (is_object($object_or_name)) {
      // First convert the object to a name, so we can get the object again
      // later. (This makes sense in case the state of the passed-in object
      // is different from what we want, i.e. $default_state.)
      $schema = ctools_export_get_schema($type);
      $key = $schema['export']['key'];
      if (empty($object_or_name->$key)) {
        trigger_error("Object has no 'name' property; code needs adjustment?");
        return;
      }
      $object_name = $object_or_name->$key;
    }

    if ($default_state) {
      $return = ctools_get_default_object($type, $object_name);
    }
    else {
      $return = is_object($object_or_name) ? $object_or_name :
        ctools_export_crud_load($type, $object_name);
    }

    if ($serialized) {
      $return = ctools_export_crud_export($type, $return);
    }
  }
  else {
    // Handle entity.
    if (!$default_state) {
      if (is_object($object_or_name)) {
        $return = $object_or_name;
      }
      else {
        $objects = entity_load($type, array($object_name));
        if (!$objects) {
          trigger_error('Failed to load entity.');
          return;
        }
        $return = reset($objects);
      }
    }
    else {
      // 'Load' all default entities from code first; return the one we need.

      /// Code copied from _entity_defaults_rebuild():

      $hook = isset($info['export']['default hook']) ? $info['export']['default hook'] : 'default_' . $type;
      $keys = $info['entity keys'] + array('module' => 'module', 'status' => 'status', 'name' => $info['entity keys']['id']);

      // Check for the existence of the module and status columns.
      if (!in_array($keys['status'], $info['schema_fields_sql']['base table']) || !in_array($keys['module'], $info['schema_fields_sql']['base table'])) {
        trigger_error("Missing database columns for the exportable entity $type as defined by entity_exportable_schema_fields(). Update the according module and run update.php!", E_USER_WARNING);
        return;
      }

      // Invoke the hook and collect default entities.
      $entities = array();
      foreach (module_implements($hook) as $module) {
        foreach ((array) module_invoke($module, $hook) as $name => $entity) {
          if (empty($entity)) {
            // We can't do anything with this and it's likely not related to the
            // object we are diff-ing now. But we'll output a warning as a
            // service to the developer who may want to know about broken code.
            trigger_error("module_invoke($module, $hook) returned empty entity for name '$name'. Is this a code error?", E_USER_WARNING);
          }
          else {
            $entity->{$keys['name']} = $name;
            $entity->{$keys['module']} = $module;
            $entities[$name] = $entity;
          }
        }
      }
      drupal_alter($hook, $entities);

      if (is_object($object_or_name)) {
        // Get the object name from the (probably non-default) passed-in entity.
        $object_name = $object_or_name->{$keys['name']};
      }

      if (!isset($entities[$object_name])) {
        trigger_error('Failed to get entity.');
        return;
      }
      $return = $entities[$object_name];
    }

    if ($serialized) {
      $return = entity_export($type, $return);
    }
  }

  return $return;
}

/**
 * Gets status of an object/entity.
 *
 * @param string $type
 *   Entity type, or table name for 'non entity, direct CTools' objects.
 * @param object|string $object_or_name
 *   (Name of) the entity / CTools object.
 *
 * @return int|null
 *   Status value or null on failure, in which trigger_error() will have been
 *   called.
 */
function ctex_diff_get_status($type, $object_or_name) {

  if (!is_object($object_or_name)) {
    $object_or_name = ctex_diff_get_object($type, $object_or_name);
    if (!isset($object_or_name)) {
      // trigger_error() has already been called.
      return;
    }
  }

  $info = entity_get_info($type);

  if (empty($info['exportable'])) {
    // Handle CTools object.
    ctools_include('export');

    if (empty($object_or_name->export_type)) {
      $schema = ctools_export_get_schema($type);
      $key = $schema['export']['key'];
      trigger_error(t('Export type not set for %o', array('%o' => $object_or_name->$key)));
      return;
    }
    return $object_or_name->export_type;
  }

  // Handle entity.
  $statuskey = isset($info['entity keys']['status']) ? $info['entity keys']['status'] : 'status';
  if (empty($object_or_name->$statuskey)) {
    trigger_error(t('Status not set for %o', array('%o' => $object_or_name->$statuskey)));
    return;
  }
  return $object_or_name->$statuskey;
}

/**
 * Form callback: perform delete/revert/enable/disable on a CTools object.
 */
function ctex_diff_object_action_form($form, $form_state, $type, $name, $action) {

  $object = ctex_diff_get_object($type, $name);
  if (!isset($object)) {
    return array(array('#markup' => t('The object does not seem to exist.')));
  }

  // Do checks.
  switch ($action) {
    case 'enable':
    case 'disable':
      // Do checks to protect against wrong input.
      ctools_include('export');
      $schema = ctools_export_get_schema($type);
      if (empty($schema['export']['can disable'])) {
        return array(array('#markup' => t('Cannot disable @type types.', array('@type' => $type))));
      }
      if (empty($object->disabled) && $action === 'enable'
          || !empty($object->disabled) && $action === 'disable') {
        // This is a messy source translation; non-english languages may need to
        // translate to e.g. "performed action @action on @type '@name'".
        return array(array('#markup' => (t("@type '@name' is already @actiond.", array('@action' => t($action), '@type' => $type, '@name' => $name)))));
      }
      $verb = $action;
      break;

    case 'delete':
      $info = entity_get_info($type);
      if (empty($info['exportable'])) {
        $statuskey = 'export_type';
      }
      else {
        $statuskey = isset($info['entity keys']['status']) ? $info['entity keys']['status'] : 'status';
      }

      if (!($object->$statuskey & EXPORT_IN_DATABASE)) {
        return array(array('#markup' => (t("Cannot revert/delete @type '@name'.", array('@type' => $type, '@name' => $name)))));
      }
      $verb = $object->$statuskey & EXPORT_IN_CODE ? 'revert' : 'delete';
      break;

    default:
      drupal_set_message(t("Action '@action' not recognized. Code error!?", array('@action' => $action)), 'error');
      return array();
  }

  $form = confirm_form($form, t("Are you sure you want to @action @type '@name'?", array('@action' => t($verb), '@type' => $type, '@name' => $name)), 'admin/structure/ctex_diff/list/' . check_plain($type));
  $form['type'] = array('#type' => 'value', '#value' => $type);
  $form['name'] = array('#type' => 'value', '#value' => $name);
  $form['action'] = array('#type' => 'value', '#value' => $verb);
  return $form;
}

/**
 * Form submit callback: perform delete/revert/enable/disable on CTools object.
 */
function ctex_diff_object_action_form_submit($form, $form_state) {
  if (!isset($form_state['values']['type']) || !is_string($form_state['values']['type']) || !isset($form_state['values']['name']) || !is_string($form_state['values']['name']) || !isset($form_state['values']['action']) || !is_string($form_state['values']['action'])) {
    drupal_set_message(t('Type/value/action not correctly set. Code error!?'), 'error');
  }
  $verb = $form_state['values']['action'];
  $type = $form_state['values']['type'];
  $name = $form_state['values']['name'];

  $info = entity_get_info($type);
  if (empty($info['exportable'])) {
    $object = ctex_diff_get_object($type, $name);
    if (!isset($object)) {
      drupal_set_message(t("The @type '@name' does not seem to exist; cannot @action.", array('@action' => t('revert/delete'), '@type' => $type, '@name' => $name)));
      return;
    }
  }

  switch ($verb) {
    case 'enable':
    case 'disable':
      // If a type has a 'status callback' it cannot necessarily deal with
      // names, so pass the object.
      ctools_export_crud_set_status($type, $object, $verb === 'disable');
      // This is a messy source translation; non-english languages may need to
      // translate like the below.
      drupal_set_message(t("@type '@name' was @actiond", array('@action' => t($verb), '@type' => $type, '@name' => $name)));
      break;

    case 'delete':
    case 'revert':
      if (empty($info['exportable'])) {
        ctools_export_crud_delete($type, $object);
      }
      else {
        entity_delete($type, $name);
      }
      drupal_set_message(t("performed '@action' action on @type '@name'", array('@action' => t($verb), '@type' => $type, '@name' => $name)));
      break;

    default:
      drupal_set_message(t("Action '@action' not recognized. Code error!?", array('@action' => $verb)), 'error');
  }
}

/**
 * Form callback containing default code version, used in page manager screens.
 */
function _ctex_diff_page_manager_code_form($form, $form_state) {
  module_load_include('inc', 'ctex_diff');
  if (isset($form_state['handler'])) {
    $code_form = ctex_diff_view_code('page_manager_handlers', $form_state['handler']->name);
  }
  else {
    $code_form = ctex_diff_view_code('page_manager_pages', $form_state['page']->subtask_id);
  }
  unset($code_form['code']['#title']);
  $form['code'] = $code_form['code'];
  // We don't need an 'update' button here.
  unset($form['buttons']['return']);
  return $form;
}

/**
 * Form callback containing diff markup, used in page manager screens.
 */
function _ctex_diff_page_manager_diff_form($form, $form_state) {
  module_load_include('inc', 'ctex_diff');
  if (isset($form_state['handler'])) {
    $form['markup'] = array('#markup' => ctex_diff_view_diff('page_manager_handlers', $form_state['handler']->name));
  }
  else {
    $form['markup'] = array('#markup' => ctex_diff_view_diff('page_manager_pages', $form_state['page']->subtask_id));
  }
  // We don't need an 'update' button here.
  unset($form['buttons']['return']);
  return $form;
}
